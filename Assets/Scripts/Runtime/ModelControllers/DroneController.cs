﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneController : MonoBehaviour
{
    public GameObject rotor1;
    public GameObject rotor2;
    public GameObject rotor3;

    public float speed = 1000f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        rotor1.transform.Rotate(0, speed*Time.deltaTime, 0);
        rotor2.transform.Rotate(0, speed*Time.deltaTime, 0);
        rotor3.transform.Rotate(0, speed*Time.deltaTime, 0);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyController : MonoBehaviour
{   

    public float speed = 100f;
    GameObject master;
    // Start is called before the first frame update
    void Start()
    {
        master = GameObject.Find("Master");
    }

    void DisableText(){
        master.SendMessage("Show","");
        Close();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            master.SendMessage("KeyPicked", this.gameObject.name);
            Invoke("DisableText", 2f);
        }
    }

    public void Close(){
        Destroy(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(speed*Time.deltaTime/2, 0, speed*Time.deltaTime);
    }
}

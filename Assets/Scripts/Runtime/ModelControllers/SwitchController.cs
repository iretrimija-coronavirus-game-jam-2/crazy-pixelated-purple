﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchController : MonoBehaviour
{
    public Material on;
    public Material off;
    public Material neutral;
    public Material neutralMid;
    private bool working = false;
    // Start is called before the first frame update
    void Start()
    {
        Close();
    }

    public void Open(){
        if(!working){
            working = true;
            Material[] matArray = GetComponent<Renderer>().materials;
            matArray[1] = neutralMid;
            matArray[2] = neutralMid;
            GetComponent<Renderer>().materials = matArray;
            Invoke("Activate",2f);
        }
    }

    public void Activate(){
        Material[] matArray = GetComponent<Renderer>().materials;
        matArray[1] = on;
        matArray[2] = neutral;
        GetComponent<Renderer>().materials = matArray;
        working = false;
    }

    public void Close(){
        if(!working){
            working = true;
            Material[] matArray = GetComponent<Renderer>().materials;
            matArray[1] = neutralMid;
            matArray[2] = neutralMid;
            GetComponent<Renderer>().materials = matArray;
            Invoke("Deactivate",2f);
        }
    }

    public void Deactivate(){
        Material[] matArray = GetComponent<Renderer>().materials;
        matArray[1] = neutral;
        matArray[2] = off;
        GetComponent<Renderer>().materials = matArray;
        working = false;
    }

    // Update is called once per frame
    void Update()
    {

    }
}

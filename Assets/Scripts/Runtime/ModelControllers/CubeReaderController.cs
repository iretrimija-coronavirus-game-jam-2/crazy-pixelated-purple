﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CubeReaderController : MonoBehaviour
{
    public GameObject[] ActivateObjects; //Activate Objects
    public GameObject[] DeactivateObjects; //Delete Objects
    public GameObject[] ActivateColliderObjects; //Activate Colliders
    public GameObject[] ActivateOpenFunction;

    public GameObject[] ActivateCloseFunction;

    // Start is called before the first frame update
    public GameObject line1;
    public GameObject line2;
    public GameObject line3;
    public GameObject line4;
    public UnityEvent OnSuccess;
    GameObject master;

    void Start()
    {
        master = GameObject.Find("Master");
    }

    void DisableText()
    {
        master.SendMessage("Show", "");
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player") && master.GetComponent<Master>().hasKey(this.gameObject.name)){
            master.SendMessage("Show", this.gameObject.name + " was inserted!!!");
            Invoke("DisableText", 2f);
            GetComponent<Collider>().enabled = false;
            for (int i = 0; i < DeactivateObjects.Length; i++)
            {
                DeactivateObjects[i].SetActive(false);
            }

            for (int i = 0; i < ActivateObjects.Length; i++)
            {
                ActivateObjects[i].SetActive(true);
            }

            for (int i = 0; i < ActivateColliderObjects.Length; i++)
            {
                ActivateColliderObjects[i].GetComponent<Collider>().enabled = true;
            }

            for (int i = 0; i < ActivateOpenFunction.Length; i++)
            {
                ActivateOpenFunction[i].SendMessage("Open");
            }

            for (int i = 0; i < ActivateCloseFunction.Length; i++)
            {
                ActivateCloseFunction[i].SendMessage("Close");
            }
            
            OnSuccess?.Invoke();
        }
    }

    void FixedUpdate()
    {
    }

    // Update is called once per frame
    void Update()
    {
        //Random.Range(0.01f,0.10f)
        line1.transform.localScale = new Vector3(Random.Range(0.01f, 0.10f), line1.transform.localScale.y,
            line1.transform.localScale.z);
        line2.transform.localScale = new Vector3(Random.Range(0.01f, 0.10f), line2.transform.localScale.y,
            line2.transform.localScale.z);
        line3.transform.localScale = new Vector3(Random.Range(0.01f, 0.10f), line3.transform.localScale.y,
            line3.transform.localScale.z);
        line4.transform.localScale = new Vector3(Random.Range(0.01f, 0.10f), line4.transform.localScale.y,
            line4.transform.localScale.z);
    }
}
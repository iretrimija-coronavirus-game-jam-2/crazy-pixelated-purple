﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    private float speed = 30f;
 
    public Transform start;
    public Transform des;
    private float fraction = 0; 
    public GameObject body;
    private bool shoot = false;

    int count = 0;

    void Start () {

    } 
    
    public void Shoot(){
        shoot=true;
        fraction=0;
        Invoke("reset",0.1f);
    }

    private void reset(){
        shoot = false;
        body.transform.position = start.position;
    }

    void FixedUpdate(){
        count++;
        if(count==50){
            count = 0;
            Shoot();
        }
    }

    void Update(){
        if (fraction < 1f && shoot) {
            fraction += Time.deltaTime * speed;
            body.transform.position = Vector3.Lerp(start.position, des.position, fraction);
        }

    }
}

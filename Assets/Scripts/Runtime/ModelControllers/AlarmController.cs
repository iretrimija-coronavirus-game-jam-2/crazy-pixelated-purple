﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlarmController : MonoBehaviour
{
    public GameObject light;
    public GameObject light2;
    private float speed = 100f;
    // Start is called before the first frame update
    void Start()
    {
        //Invoke("Open",3f);
    }

    public void Open(){
        light.SetActive(true);
        light2.SetActive(true);
    }

    public void Close(){
        light.SetActive(false);
        light2.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

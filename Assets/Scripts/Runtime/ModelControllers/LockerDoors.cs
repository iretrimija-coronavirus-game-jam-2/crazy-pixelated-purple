﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockerDoors : MonoBehaviour
{
    public GameObject Left;
    public GameObject Right;
    // Start is called before the first frame update
    public Transform leftfrom;
    public Transform lefto;

    public Transform rightfrom;
    public Transform rightto;

    private float timeCount = 0.0f;

    private bool go = false;
    private bool first = false;
    void Start()
    {

    }

    public void Open(){
        first = true;
        go = true;
        timeCount = 0.0f;
        Invoke("Close",5f);
    }

    public void Close(){
        first = true;
        go = false;
        timeCount = 0.0f;
    }
    // Update is called once per frame
    void Update()
    {
        if(first){
            if(go){
                Left.transform.rotation = Quaternion.Slerp(leftfrom.rotation, lefto.rotation, timeCount);
                Right.transform.rotation = Quaternion.Slerp(rightfrom.rotation, rightto.rotation, timeCount);
                timeCount = timeCount + Time.deltaTime;
            }else{
                Left.transform.rotation = Quaternion.Slerp(lefto.rotation, leftfrom.rotation, timeCount);
                Right.transform.rotation = Quaternion.Slerp(rightto.rotation, rightfrom.rotation, timeCount);
                timeCount = timeCount + Time.deltaTime;
            }
        }
    }
}

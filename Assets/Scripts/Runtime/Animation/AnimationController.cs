﻿using CPP.Character.InputComponent;
using UnityEngine;

namespace CPP.Animation
{
    public class AnimationController : MonoBehaviour
    {
        #region public constants 
        
        public static readonly int HorizontalAnimKey = Animator.StringToHash("Horizontal");
        public static readonly int VerticalAnimKey = Animator.StringToHash("Vertical");
        
        #endregion
        
        #region protected variables
        
        protected IMoveInput input;
        protected Animator animator;
        protected Transform internalTransform;
        protected float verticalDot;
        protected float horizontalDot;

        #endregion
        
        #region protected methods

        protected void Awake()
        {
            input = GetComponent<IMoveInput>();
            internalTransform = GetComponent<Transform>();
            animator = GetComponent<Animator>();
        }

        protected void Update()
        {
            verticalDot = Vector3.Dot(internalTransform.forward, input.MoveDirection);
            horizontalDot = Vector3.Dot(internalTransform.right, input.MoveDirection);
            
            animator.SetFloat(HorizontalAnimKey, horizontalDot);
            animator.SetFloat(VerticalAnimKey, verticalDot);
        }
        
        #endregion
    }
}
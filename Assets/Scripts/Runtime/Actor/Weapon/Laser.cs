﻿using System;
using System.Collections;
using CPP.Character;
using CPP.Character.Combat;
using UnityEngine;

namespace CPP.Actor.Weapon
{
    public class Laser : MonoBehaviour, IHit
    {
        #region public constants

        public const float TIME_TO_WAIT_TO_DESTROY = 8f;
        
        #endregion
        
        #region public variables

        public int damage = 1;
        public int Damage { get; set; }
        
        #endregion
        
        #region protected methods
        
        protected Rigidbody internalRidbody;
        protected CharacterHealth characterHealth;
        protected Coroutine coroutine;
        
        #endregion
        
        #region protected methods

        protected void Start()
        {
            // Initialization
            internalRidbody = GetComponentInChildren<Rigidbody>();
            Damage = damage;
            coroutine = StartCoroutine(DestroyOnTime());
        }

        protected void OnTriggerEnter(Collider other)
        {
            StopCoroutine(coroutine);
            characterHealth = other.GetComponent<CharacterHealth>();
            if(characterHealth != null)
                characterHealth.AddDamage(Damage);
            
            Destroy(gameObject);
        }

        protected IEnumerator DestroyOnTime()
        {
            yield return new WaitForSeconds(TIME_TO_WAIT_TO_DESTROY);
            Destroy(gameObject);
        }

        #endregion

    }
}
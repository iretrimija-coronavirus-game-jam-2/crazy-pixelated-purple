using UnityEngine;

namespace CPP.Actor
{
    public interface IInteractable
    {
        void Interact(GameObject executorObject);
    }
}
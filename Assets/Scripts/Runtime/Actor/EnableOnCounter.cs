﻿using UnityEngine;

namespace CPP.Actor
{
    public class EnableOnCounter : MonoBehaviour
    {
        public int counter = 2;
        public GameObject[] objectsToEnable;

        protected int currentCounter = 0;

        public void AddCounter()
        {
            ++currentCounter;
            if (currentCounter >= counter)
            {
                counter = 0;
                foreach (GameObject objectToEnable in objectsToEnable)
                {
                    objectToEnable.SetActive(true); 
                }
            }
        }
    }
}
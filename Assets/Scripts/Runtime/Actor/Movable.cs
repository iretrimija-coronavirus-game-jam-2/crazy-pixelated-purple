﻿using System;
using System.Collections;
using CPP.Actor;
using UnityEngine;

namespace CPP.Actor
{
    [RequireComponent(typeof(Rigidbody))]
    public class Movable : MonoBehaviour, IMovable
    {
        #region public variables

        public float magnetForce = 1000;

        #endregion

        #region protected variables

        protected Rigidbody internalRigidbody;
        protected Coroutine coroutine = null;
        protected bool magnetLoop = true;
        protected bool interact = true;

        #endregion

        #region public methods

        public void Interact(GameObject executorObject)
        {
            if (interact && coroutine == null)
            {
                magnetLoop = true;
                coroutine = StartCoroutine(MagnetForce(executorObject));
            }
            else
            {
                magnetLoop = false;
            }
        }

        public void ToogleInteract()
        {
            interact = !interact;
            if (!interact)
            {
                magnetLoop = false;
                if (coroutine != null)
                {
                    StopCoroutine(coroutine);
                    coroutine = null;
                }
            }
        }

        #endregion

        #region protected methods

        protected void Awake()
        {
            internalRigidbody = GetComponent<Rigidbody>();
        }

        protected IEnumerator MagnetForce(GameObject executorObject)
        {
            internalRigidbody.isKinematic = false;
            while (magnetLoop)
            {
                internalRigidbody.velocity =
                    (executorObject.transform.position -
                     (internalRigidbody.transform.position + internalRigidbody.centerOfMass)) *
                    (magnetForce * Time.deltaTime);


                // Initialization
                Transform player = executorObject.transform.parent;
                Vector3 lookRotation = (transform.position - player.position).normalized;
                lookRotation.y = 0f;

                // Set proper rotation
                player.GetComponent<Rigidbody>().rotation =
                    Quaternion.LookRotation(lookRotation);

                yield return null;
            }

            internalRigidbody.velocity = Vector3.zero;
            internalRigidbody.isKinematic = true;
            coroutine = null;
        }

        #endregion
    }
}
﻿using UnityEngine;

namespace CPP.Actor
{
    public class CubePuzzleCube : MonoBehaviour
    {
        #region public variables

        public GameObject cubeOn;
        public GameObject cubeRed;
        public GameObject cubeGreen;

        #endregion
        
        #region public methods

        public void ChangeToOn()
        {
            cubeOn.SetActive(true);
            cubeRed.SetActive(false);
            cubeGreen.SetActive(false);
        }

        public void ChangeToRed()
        {
            cubeOn.SetActive(false);
            cubeRed.SetActive(true);
            cubeGreen.SetActive(false);
        } 
        
        public void ChangeToGreen()
        {
            cubeOn.SetActive(false);
            cubeRed.SetActive(false);
            cubeGreen.SetActive(true);
        } 
        
        #endregion
    }
}
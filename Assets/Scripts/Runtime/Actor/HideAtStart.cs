﻿using UnityEngine;

namespace CPP.Actor
{
    public class HideAtStart : MonoBehaviour
    {
        protected void Start()
        {
            gameObject.SetActive(false);
        }
    }
}
﻿using System.Collections;
using UnityEngine;

namespace CPP.Actor
{
    public class HideOnTime : MonoBehaviour
    {
        public float timeToHideAtStart = 3f;

        protected void OnEnable()
        {
            StartCoroutine(HideOnTimeCoroutine());
        }

        protected IEnumerator HideOnTimeCoroutine()
        {
            yield return new WaitForSeconds(timeToHideAtStart);
            gameObject.SetActive(false);
        }
    }
}
﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace CPP.Actor
{
    [RequireComponent(typeof(BoxCollider))]
    public class CubePuzzle : MonoBehaviour
    {
        #region public variables

        public bool correctPosition = false;
        public float timeToCheckTheCube = 1.5f;
        public CubesPuzzle cubesPuzzle;

        #endregion
        
        #region public variables

        protected bool cubeInside = false;
        protected bool finished = false;
        protected CubePuzzleCube cubeInsideObject;
        protected Coroutine check = null;
        
        #endregion

        #region public events

        [Header("Events")] 
        public UnityEvent OnCorrectCube;
        public UnityEvent OnIncorrectCube;

        #endregion
        
        #region public methods

        public bool IsFinished()
        {
            return finished;
        }
        
        #endregion

        #region protected methods

        protected void OnTriggerEnter(Collider other)
        {
            if (other.tag.Equals(CubesPuzzle.cubeTag) && !finished && check == null)
            {
                Debug.Log("OnTriggerEnter " + other.gameObject.name + " with tag " + other.tag);
                cubeInside = true;
                cubeInsideObject = other.GetComponentInParent<CubePuzzleCube>();
                if (cubeInsideObject != null && check == null)
                    check = StartCoroutine(CheckTime(other.gameObject));
            }
        }

        protected void OnTriggerExit(Collider other)
        {
            if (other.tag.Equals(CubesPuzzle.cubeTag) && !finished)
            {
                Debug.Log("OnTriggerExit " + other.gameObject.name + " with tag " + other.tag);
                cubeInsideObject = null;
                if(check != null)
                    StopCoroutine(check);
                check = null;
                cubeInside = false;
            }
        }

        protected IEnumerator CheckTime(GameObject interactableObject)
        {
            yield return new WaitForSeconds(timeToCheckTheCube);
            if (cubeInside)
            {
                if (correctPosition)
                {
                    finished = true;
                    interactableObject.GetComponent<Movable>().ToogleInteract();
                    OnCorrectCube?.Invoke();
                    cubesPuzzle.OnCorrectCube?.Invoke();
                    cubeInsideObject.ChangeToGreen();
                    cubesPuzzle.CheckAllCubes();
                }
                else
                {
                    interactableObject.GetComponent<Movable>().ToogleInteract();
                    OnIncorrectCube?.Invoke();
                    cubesPuzzle.OnIncorrectCube?.Invoke();
                    cubeInsideObject.ChangeToRed();
                    yield return new WaitForSeconds(2f);
                    cubeInsideObject.ChangeToOn();
                    interactableObject.GetComponent<Movable>().ToogleInteract();
                }
            }
            
            check = null;
        }

        #endregion
    }
}
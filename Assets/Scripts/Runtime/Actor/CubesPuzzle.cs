﻿using EasyButtons;
using UnityEngine;
using UnityEngine.Events;

namespace CPP.Actor
{
    public class CubesPuzzle : MonoBehaviour
    {
        #region public constants

        public const string cubeTag = "CubePuzzle";
        
        #endregion
        
        #region public variables
        
        public float timeToCheckTheCube = 1.5f;
        public int numberOfKeys = 3;

        public CubePuzzle[] cubesZones;

        #endregion

        #region public events

        [Header("Events")] 
        public UnityEvent OnCorrectCube;
        public UnityEvent OnIncorrectCube;
        public UnityEvent OnCorrectCubes;

        #endregion

        #region protected variables

        protected CubePuzzle cubePuzzle;
        protected int currentKeys = 0;

        #endregion
        
        #region public methods

        public void CheckAllCubes()
        {
            if (currentKeys < numberOfKeys)
                return;
            
            foreach (CubePuzzle cubeZone in cubesZones)
            {
                if (cubeZone.correctPosition && !cubeZone.IsFinished())
                    return;
            }
            OnCorrectCubes?.Invoke();
            foreach (CubePuzzle cubeZone in cubesZones)
                cubeZone.gameObject.SetActive(false);
        }

        public void AddKey()
        {
            ++currentKeys;
            CheckAllCubes();
        }
        
        #endregion

        #region protected methods

        [Button("Set children cubes")]
        public void GetCubesChildren()
        {
            cubesZones = new CubePuzzle[transform.childCount];
            for (byte index = 0; index < transform.childCount; ++index)
            {
                cubePuzzle = transform.GetChild(index).GetComponent<CubePuzzle>();
                if (cubePuzzle == null)
                    cubePuzzle = transform.GetChild(index).gameObject.AddComponent<CubePuzzle>();

                cubesZones[index] = cubePuzzle;
                cubePuzzle.GetComponent<Collider>().isTrigger = true;
                cubePuzzle.timeToCheckTheCube = timeToCheckTheCube;
                cubePuzzle.cubesPuzzle = this;
            }
        }

        #endregion
    }
}
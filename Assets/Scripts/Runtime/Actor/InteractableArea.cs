﻿using CPP.Character.Command;
using UnityEngine;

namespace CPP.Actor
{
    public class InteractableArea : MonoBehaviour
    {
        #region protected methods

        protected GameObject objectInteractable = null;
        protected IInteractable interactable = null;

        #endregion
        
        #region public methods

        public void Interact()
        {
            if (objectInteractable != null)
            {
                interactable = objectInteractable.GetComponent<IInteractable>();
                interactable.Interact(this.gameObject);
                GameObject.Find("Master").SendMessage("Show","");
            }
        }
        
        #endregion

        #region protected methods

        protected void OnTriggerEnter(Collider other)
        {
            if 
            (
                other.gameObject.layer == LayerMask.NameToLayer(InteractCommand.LayerName) &&
                objectInteractable == null
            )
            {
                objectInteractable = other.gameObject;
                GameObject.Find("Master").SendMessage("Show","Press [E]");
            }
        }

        protected void OnTriggerExit(Collider other)
        {
            if 
            (
                other.gameObject.layer == LayerMask.NameToLayer(InteractCommand.LayerName) &&
                objectInteractable != null
            )
            {
                //Debug.Log("OnTriggerExit " + other.name);
                objectInteractable = null;
                GameObject.Find("Master").SendMessage("Show","");
            }
        }

        #endregion
    }
}
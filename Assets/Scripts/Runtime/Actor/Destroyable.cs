﻿using System;
using System.Collections;
using CPP.Actor;
using UnityEngine;
using UnityEngine.Events;

namespace CPP.Actor
{
    [RequireComponent(typeof(Rigidbody))]
    public class Destroyable : MonoBehaviour, IDestroyable
    {
        #region public variables
        public GameObject[] ActivateObjects;                //Activate Objects
        public GameObject[] DeactivateObjects;              //Delete Objects
        public GameObject[] ActivateColliderObjects;        //Activate Colliders
        public GameObject[] ActivateOpenFunction;
        public GameObject[] ActivateCloseFunction;

        public Boolean deletable = true;                   
        public Boolean desactivable = true;                 //true = only once interactable

        public int finalcubes;
        public UnityEvent OnInteract;
        #endregion

        #region protected variables


        #endregion

        #region public methods

        public void Interact(GameObject executorObject)
        {
            for(int i = 0 ; i<DeactivateObjects.Length ; i++){
                DeactivateObjects[i].SetActive(false);
            }

            for(int i = 0 ; i<ActivateObjects.Length ; i++){
                ActivateObjects[i].SetActive(true);
            }

            for(int i = 0 ; i<ActivateColliderObjects.Length ; i++){
                ActivateColliderObjects[i].GetComponent<Collider>().enabled = true;
            }

            for(int i = 0 ; i<ActivateOpenFunction.Length ; i++){
                ActivateOpenFunction[i].SendMessage("Open");
            }

            for(int i = 0 ; i<ActivateCloseFunction.Length ; i++){
                ActivateCloseFunction[i].SendMessage("Close");
            }
            
            OnInteract?.Invoke();
            
            if(deletable){
                Destroy(this.gameObject);
            }else{                                          //If cant be destroyed will be deactivated to avoid showing messages on the ui                
                if(desactivable){GetComponent<Collider>().enabled = false;}    
            }         
        }

        #endregion

        #region protected methods

        protected void Awake()
        {
            
        }

        #endregion
    }
}
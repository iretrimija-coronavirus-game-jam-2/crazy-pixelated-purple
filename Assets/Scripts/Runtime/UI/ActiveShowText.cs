﻿using System;
using UnityEngine;

namespace CPP.UI
{
    public class ActiveShowText : MonoBehaviour
    {
        #region public variables

        public string textEnable = "";
        public string textDisable = "";

        #endregion

        #region protected variables

        protected Master master;

        #endregion

        #region protected methods

        protected void GetMasterComponent()
        {
            master = FindObjectOfType<Master>();
        }

        protected void OnEnable()
        {
            if (!String.IsNullOrEmpty(textEnable))
            {
                if (master == null)
                    GetMasterComponent();

                master.Show(textEnable);
            }
        }

        protected void OnDisable()
        {
            if (!String.IsNullOrEmpty(textDisable))
            {
                master.Show(textDisable);
            }
        }

        #endregion
    }
}
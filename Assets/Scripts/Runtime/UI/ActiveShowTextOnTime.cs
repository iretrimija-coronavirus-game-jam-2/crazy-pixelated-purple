﻿using System;
using System.Collections;
using UnityEngine;

namespace CPP.UI
{
    public class ActiveShowTextOnTime : ActiveShowText 
    {
        #region public variables

        public float timeToExecute = 1f;

        #endregion

        #region protected methods

        protected void OnEnable()
        {
            StartCoroutine(ExecuteOnTime(() => base.OnEnable(), timeToExecute));
        }

        protected void OnDisable()
        {
            StartCoroutine(ExecuteOnTime(() => base.OnDisable(), timeToExecute));
        }

        protected IEnumerator ExecuteOnTime(Action callback, float timeToWait)
        {
            yield return new WaitForSeconds(timeToWait);
            callback.Invoke();
        }

        #endregion
    }
}
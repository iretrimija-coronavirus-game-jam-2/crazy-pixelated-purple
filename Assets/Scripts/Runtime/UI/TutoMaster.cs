﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutoMaster : MonoBehaviour
{
    private Text showtext;
    public bool PCOn = false;
    public bool StoryRead = false;
    public bool KeyBlue = false;
    public bool KeyGreen = false;
    public bool KeyPink = false;
    public bool KeyRed = false;
    public bool KeyOff = false;
    // Start is called before the first frame update
    void Start()
    {
        showtext = GameObject.Find("ShowTutoText").GetComponent<UnityEngine.UI.Text>();
    }

    public void InteractTuto(string obj)
    {
        switch (obj)
        {
            case "Wall":
                showtext.text = "I can't climb this " + obj + " . I need some help";
                break;
            case "Computer":
                showtext.text = "mmm... It seems there is some information in this " + obj; //TODO
                Invoke("Story",4f);
                break;
            case "Gun":
                showtext.text = "I can use this " + obj + ". Aim with RIGHT CLICK and shoot with LEFT CLICK";
                break;
            case "CubeOnA":
                showtext.text = "I can PRESS E to use my magnetic power";
                break;
        }
    }

    ////Return if key was picked
    //public bool hasKey(string key)
    //{
    //    key = key.Replace("Reader", "");
    //    bool val = false;
    //    switch (key)
    //    {
    //        case "KeyGreen":
    //            val = KeyGreen;
    //            break;
    //        case "KeyBlue":
    //            val = KeyBlue;
    //            break;
    //        case "KeyPink":
    //            val = KeyPink;
    //            break;
    //        case "KeyRed":
    //            val = KeyRed;
    //            break;
    //        case "KeyOff":
    //            val = KeyOff;
    //            break;
    //    }
    //    if (!val) { showtext.text = key + " Not Found!!!"; Invoke("DisableText", 3f); }
    //    return val;
    //}

    public void DisableText()
    {
        showtext.text = "";
    }

    public void Show(string text)
    {
        showtext.text = text;
    }
    
    public void Story()
    {
        showtext.text = "Do you want to read this file? Y/N"; //TODO
        PCOn = true;
    }

    void Update()
    {
        if(PCOn == true && StoryRead == false && Input.GetKeyDown(KeyCode.Y))
        {
            StoryRead = true;
            showtext.text = "TELL STORY"; //TODO
            Invoke("DisableText", 10f);
            StartCoroutine("PlayOnOff");
        }
        else if(PCOn == true && StoryRead == false && Input.GetKeyDown(KeyCode.N))
        {
            showtext.text = "SEE YOU NEXT TIME"; //TODO
            Invoke("DisableText", 10f);
            StartCoroutine("PlayOnOff");
        }
    }

    IEnumerator PlayOnOff()
    {
        yield return new WaitForSeconds(2f);
        if(Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
        yield return new WaitForSeconds(2f);
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
        }
    }
}

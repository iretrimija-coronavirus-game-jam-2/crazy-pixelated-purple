﻿using UnityEngine;
using UnityEngine.UI;

public class Master : MonoBehaviour
{
    private Text showtext;
    public bool KeyBlue = false;
    public bool KeyGreen = false;
    public bool KeyPink = false;
    public bool KeyRed = false;

    public bool KeyOff = false;

    // Start is called before the first frame update
    void Start()
    {
        showtext = GameObject.Find("Showtext").GetComponent<Text>();
    }

    public void KeyPicked(string key)
    {
        switch (key)
        {
            case "KeyGreen":
                KeyGreen = true;
                break;
            case "KeyBlue":
                KeyBlue = true;
                break;
            case "KeyPink":
                KeyPink = true;
                break;
            case "KeyRed":
                KeyRed = true;
                break;
            case "KeyOff":
                KeyOff = true;
                break;
        }

        showtext.text = key + " was picked";
    }

    //Return if key was picked
    public bool hasKey(string key)
    {
        key = key.Replace("Reader", "");
        bool val = false;
        switch (key)
        {
            case "KeyGreen":
                val = KeyGreen;
                break;
            case "KeyBlue":
                val = KeyBlue;
                break;
            case "KeyPink":
                val = KeyPink;
                break;
            case "KeyRed":
                val = KeyRed;
                break;
            case "KeyOff":
                val = KeyOff;
                break;
        }

        if (!val)
        {
            showtext.text = key + " Not Found!!!";
            Invoke("DisableText", 3f);
        }

        return val;
    }

    public void DisableText()
    {
        showtext.text = "";
    }

    public void Show(string text)
    {
        showtext.text = text;
    }

    // Update is called once per frame
    void Update()
    {
    }
}
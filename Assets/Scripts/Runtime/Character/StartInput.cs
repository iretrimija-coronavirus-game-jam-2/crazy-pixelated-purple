﻿using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace CPP.Camera
{
    public class StartInput : MonoBehaviour
    {
        #region public variables

        public float activateInSeconds = 10f;
        public string sceneToLoadName = "FACTORY";

        #endregion

        #region protected variables

        protected InputMaster input;
        protected bool load = false;

        #endregion

        #region public methods

        public void InteractButtonInput(InputAction.CallbackContext context)
        {
            if(load)
                input.Player.Interact.performed -= InteractButtonInput;
                input.Disable();
                SceneManager.LoadScene(sceneToLoadName);
        }

        #endregion

        #region protected methods

        protected void Awake()
        {
            // Initialize
            input = new InputMaster();
            
            // Subscribe to events
            input.Player.Interact.performed += InteractButtonInput;
            
            StartCoroutine(EnableCoroutine());
        }
        
        protected void OnEnable()
        {
            input.Enable();
        }

        protected IEnumerator EnableCoroutine()
        {
            yield return new WaitForSeconds(activateInSeconds);

            load = true;
            Debug.Log("Press E to load " + sceneToLoadName);
        }

        #endregion
    }
}
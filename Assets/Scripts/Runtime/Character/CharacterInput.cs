﻿using CPP.Character.Command;
using CPP.Character.InputComponent;
using UnityEngine;
using UnityEngine.InputSystem;

namespace CPP.Camera
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(InputMoveCommand))]
    [RequireComponent(typeof(MouseRotationCommand))]
    [RequireComponent(typeof(InteractCommand))]
    public class CharacterInput :
        MonoBehaviour,
        IMoveInput,
        IRotationInput,
        IInteractInput,
        IAttackInput,
        IJumpInput
    {
        #region public variables

        public Command interactCommand;
        public Command movementCommand;
        public Command mouseRotationCommand;
        public Command attackCommand;
        public Command jumpCommand;

        public Vector3 MoveDirection { get; protected set; }
        public Vector2 ScreenPoint { get; set; }
        public Vector3 AttackDirection { get; protected set; }
        public bool IsPressingRotation { get; set; }
        public bool IsPressingInteract { get; protected set; }
        public bool MayShoot { get; protected set; }
        public float JumpButtonIncrement { get; protected set; }

        #endregion

        #region protected variables

        protected InputMaster input;

        #endregion

        #region public methods

        public void MoveInput(InputAction.CallbackContext context)
        {
            Vector2 value = context.ReadValue<Vector2>();

            //camera forward and right vectors:
            UnityEngine.Camera camera = UnityEngine.Camera.main;
            Vector3 forward = camera.transform.forward;
            Vector3 right = camera.transform.right;

            //project forward and right vectors on the horizontal plane (y = 0)
            forward.y = 0f;
            right.y = 0f;
            forward.Normalize();
            right.Normalize();

            // Calculate direction vector
            MoveDirection = forward * value.y + right * value.x;

            if (movementCommand != null)
                movementCommand.Execute();
        }

        public void MouseAimInput(InputAction.CallbackContext context)
        {
            ScreenPoint = context.ReadValue<Vector2>();
            if (mouseRotationCommand != null)
                mouseRotationCommand.Execute();
        }

        public void InteractButtonInput(InputAction.CallbackContext context)
        {
            float value = context.ReadValue<float>();
            IsPressingInteract = value >= 0.15f;
            if (interactCommand != null && IsPressingInteract)
            {
                interactCommand.Execute();
            }
        }

        public void ShootAimInput(InputAction.CallbackContext context)
        {
            var value = context.ReadValue<float>();
            MayShoot = IsPressingRotation = value >= 0.15f;
        }

        public void ShootInput(InputAction.CallbackContext context)
        {
            if (attackCommand != null)
            {
                attackCommand.Execute();
                SoundManager.PlaySound("playerShoot");
            }
        }

        public void JumpInput(InputAction.CallbackContext context)
        {
            var value = context.ReadValue<float>();
            if (jumpCommand != null && value >= 0.15f)
            {
                JumpButtonIncrement = value;
                jumpCommand.Execute();
            }
        }

        [ContextMenu("AddShootInput")]
        public void AddShootInput()
        {
            input.Player.Shoot.performed += ShootInput;
        }
        
        [ContextMenu("RemoveShootInput")]
        public void RemoveShootInput()
        {
            input.Player.Shoot.performed -= ShootInput;
        }

        #endregion

        #region protected methods

        protected void Awake()
        {
            // Initialize
            input = new InputMaster();
            MoveDirection = new Vector3(0, 0, 0);

            // Subscribe to events
            input.Player.Movement.performed += MoveInput;
            input.Player.Movement.canceled += MoveInput;
            input.Player.MouseAim.performed += MouseAimInput;
            input.Player.MouseAim.canceled += MouseAimInput;
            input.Player.AimShoot.performed += ShootAimInput;
            input.Player.AimShoot.canceled += ShootAimInput;
            input.Player.Jump.performed += JumpInput;
            input.Player.Interact.started += InteractButtonInput;
        }

        protected void OnEnable()
        {
            input.Enable();
        }

        protected void OnDisable()
        {
            input.Disable();
        }

        protected void Reset()
        {
            interactCommand = GetComponent<InteractCommand>();
            movementCommand = GetComponent<InputMoveCommand>();
            mouseRotationCommand = GetComponent<MouseRotationCommand>();
            attackCommand = GetComponent<AttackCommand>();
            jumpCommand = GetComponent<JumpCommand>();
        }

        protected void Start()
        {
            Reset();
        }

        #endregion
    }
}
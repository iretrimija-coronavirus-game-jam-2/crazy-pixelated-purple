﻿using System;
using CPP.Character.Health;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace CPP.Character
{
    public class CharacterHealth : MonoBehaviour
    {
        #region Events

        public UnityEvent OnAddHealth;
        public UnityEvent OnRemoveHealth;
        public UnityEvent OnDeath;
        public GameObject master;
        public Text text;
        public GameObject damageplane;
        public GameObject healthplane;

        #endregion
        
        #region public variables

        public HealthDataScriptableObject data;

        #endregion
        
        #region protected variables

        public int currentHealth;
        
        #endregion
        
        #region public methods

        public void AddHealth()
        {
            AddHealth(1); 
        }

        public void AddHealth(int healthPoint)
        {
            healthPoint = 10;
            currentHealth = Mathf.Clamp(currentHealth + healthPoint, 0, data.maxHealth);
            if(text != null)
                text.text = "" + currentHealth;
            OnAddHealth?.Invoke();
            if(healthplane != null)
                healthplane.SetActive(true);
            Invoke("DisablePlanes",0.3f);
        }
        
        public void AddDamage()
        {
            AddDamage(1); 
        }

         void OnTriggerEnter(Collider other)
        {
            if (other.tag.Equals("Player"))
            {
                master.SendMessage("KeyPicked", this.gameObject.name);
                Invoke("DisableText", 2f);
            }
        }        


        public void AddDamage(int healthPointsToDecrease)
        {
            currentHealth = Mathf.Clamp(currentHealth - healthPointsToDecrease, 0, data.maxHealth);
            if(text != null)
                text.text = "" + currentHealth;
            OnRemoveHealth?.Invoke();
            if(damageplane != null)
                damageplane.SetActive(true);
            Invoke("DisablePlanes",0.3f);
            if (currentHealth < 1)
                OnDeath?.Invoke();
        }
        
        void DisablePlanes(){
            if(damageplane != null)
                damageplane.SetActive(false);
            if(healthplane != null)
                healthplane.SetActive(false);
        }

        #endregion
        
        #region protected methods

        protected void Start()
        {
            // Initialization
            currentHealth = data.maxHealth;
            if(text != null)
                text.text = "" + currentHealth;
        }

        #endregion
    }
}
using CPP.Actor;
using UnityEngine;

namespace CPP.Character.Command
{
    public class InteractCommand : Command
    {
        #region public constans

        public const string LayerName = "Interactable";

        #endregion
        
        #region public variables
        
        public InteractableArea interactableArea;
        
        #endregion

        #region protected variables

        protected Transform internalTransform;
        //protected IInteractable interactedWith;

        #endregion

        #region public methods

        public override void Execute()
        {
            /*Physics.Raycast(internalTransform.position + Vector3.up,
                internalTransform.forward, out var hit, 2f,
                LayerMask.GetMask(LayerName));

            Debug.Log($"{gameObject.name} is trying to interact!");
            if (hit.collider == null) return;
            Debug.Log($"{gameObject.name} is interacted with {hit.collider.name}");
            
            interactedWith = hit.collider.GetComponent<IInteractable>();
            interactedWith?.Interact();*/
            
            interactableArea.Interact();
        }

        #endregion

        #region protected methods

        protected void Awake()
        {
            internalTransform = transform;
            if (interactableArea == null)
                interactableArea = GetComponent<InteractableArea>();
        }

        #endregion
    }
}
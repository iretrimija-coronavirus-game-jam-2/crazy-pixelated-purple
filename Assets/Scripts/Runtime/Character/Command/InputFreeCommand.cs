using System.Collections;
using CPP.Character.InputComponent;
using UnityEngine;
using UnityEngine.Events;

namespace CPP.Character.Command
{
    public class InputFreeCommand : Command
    {
        #region public variables

        public GameObject[] objectToRelease = new GameObject[0];
        public GameObject[] objectToReplace = new GameObject[0];
        public int timesToTap = 3;
        public UnityEvent OnFinish;

        #endregion

        #region protected variables

        protected int[] objectToReleaseTaps;
        protected int objectToReleaseIndex = 0;

        #endregion

        #region public methods

        public override void Execute()
        {
            if (objectToReleaseIndex < objectToReleaseTaps.Length)
            {
                --objectToReleaseTaps[objectToReleaseIndex];
                if (objectToReleaseTaps[objectToReleaseIndex] == 0)
                {
                    objectToRelease[objectToReleaseIndex].SetActive(false);
                    objectToReplace[objectToReleaseIndex].SetActive(true);
                    ++objectToReleaseIndex;

                    if (objectToRelease.Length == objectToReleaseIndex)
                    {
                        OnFinish?.Invoke();
                    }
                }
            }
        }

        #endregion

        #region protected methods

        protected void Awake()
        {
            // Initialization
            objectToReleaseTaps = new int[objectToRelease.Length];
            for (byte index = 0; index < objectToReleaseTaps.Length; ++index)
                objectToReleaseTaps[index] = timesToTap;
        }

        #endregion
    }
}
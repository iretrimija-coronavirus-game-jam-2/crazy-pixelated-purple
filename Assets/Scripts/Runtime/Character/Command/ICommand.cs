﻿namespace CPP.Character.Command
{
    public interface ICommand
    {
        void Execute();
    }
}
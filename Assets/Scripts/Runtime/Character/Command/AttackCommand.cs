﻿using CPP.Character.Combat;
using CPP.Character.InputComponent;
using UnityEngine;

namespace CPP.Character.Command
{
    public class AttackCommand : Command
    {
        #region public variables

        public Attack attack;
        
        #endregion
        
        #region protected variables

        protected IAttackInput attackInput;

        #endregion

        #region public methods

        public override void Execute()
        {
            if (attackInput.MayShoot)
            {
                attack.ExecuteAttack(attackInput);
            }
        }

        #endregion

        #region protected methods

        protected void Awake()
        {
            attackInput = GetComponent<IAttackInput>();
        }

        #endregion
    }
}
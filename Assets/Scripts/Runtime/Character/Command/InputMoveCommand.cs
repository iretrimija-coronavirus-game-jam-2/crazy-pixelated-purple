using System.Collections;
using CPP.Character.InputComponent;
using UnityEngine;

namespace CPP.Character.Command
{
    public class InputMoveCommand : Command
    {
        #region public variables

        public float turnSmoothing;
        public AnimationCurve speed;

        #endregion

        #region protected variables

        protected Rigidbody internalRigidbody;
        protected IMoveInput move;
        protected IRotationInput rotate;
        protected Coroutine moveCoroutine;
        protected Coroutine rotateCoroutine;
        protected Transform internalTransform;
        protected float moveTime;

        #endregion

        #region public methods

        public override void Execute()
        {
            if (moveCoroutine == null)
                moveCoroutine = StartCoroutine(Move());
            if (rotateCoroutine == null)
                rotateCoroutine = StartCoroutine(Rotate());
        }

        #endregion

        #region protected methods

        protected void Awake()
        {
            internalRigidbody = GetComponent<Rigidbody>();
            move = GetComponent<IMoveInput>();
            rotate = GetComponent<IRotationInput>();
            internalTransform = transform;
        }

        protected IEnumerator Move()
        {
            while (move.MoveDirection != Vector3.zero)
            {
                moveTime = (Time.fixedDeltaTime * speed.Evaluate(move.MoveDirection.magnitude));
                internalRigidbody.MovePosition(internalTransform.position + move.MoveDirection * moveTime);
                yield return null;
            }

            moveCoroutine = null;
        }

        protected IEnumerator Rotate()
        {
            float time = 0.0f;
            while (move.MoveDirection != Vector3.zero)
            {
                yield return new WaitUntil(() => (rotate.ScreenPoint == Vector2.zero));
                if (move.MoveDirection == Vector3.zero) continue;
                if (move.MoveDirection.magnitude <= 0.5f)
                {
                    var targetRotation = Quaternion.LookRotation(
                        move.MoveDirection,
                        Vector3.up
                    );
                    time += Time.fixedDeltaTime * turnSmoothing * move.MoveDirection.magnitude;
                    var newRotation = Quaternion.Lerp(
                        internalRigidbody.rotation,
                        targetRotation,
                        time
                    );
                    internalRigidbody.MoveRotation(newRotation);
                }
                else
                    internalRigidbody.MoveRotation(
                        Quaternion.LookRotation(
                            move.MoveDirection,
                            Vector3.up
                        ));
            }

            rotateCoroutine = null;
        }

        #endregion
    }
}
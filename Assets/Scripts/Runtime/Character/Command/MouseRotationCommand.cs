using System.Collections;
using CPP.Camera;
using CPP.Character.InputComponent;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;

namespace CPP.Character.Command
{
    public class MouseRotationCommand : Command
    {
        #region protected variables

        protected IRotationInput rotate;
        protected Rigidbody internalRigidbody;
        protected TopDownCamera topDownCamera;
        protected UnityEngine.Camera camera;
        protected Coroutine coroutine;
        protected Vector3 lookRotation;
        protected Vector3 cameraRotation;
        protected Vector3 mousePosition;
        protected Ray ray;
        protected RaycastHit raycastHit;
        protected Animator anim;

        #endregion

        #region public methods

        public override void Execute()
        {
            if (coroutine == null)
                //anim.SetBool("Aim", true);
                coroutine = StartCoroutine(Rotate());
        }

        #endregion

        #region protected methods

        protected void Awake()
        {
            rotate = GetComponent<IRotationInput>();
            internalRigidbody = GetComponent<Rigidbody>();
            topDownCamera = UnityEngine.Camera.main.GetComponent<TopDownCamera>();
            camera = topDownCamera.GetComponent<UnityEngine.Camera>();
            anim = GetComponent<Animator>();
        }

        protected IEnumerator Rotate()
        {
            while (rotate.IsPressingRotation)
            {
                anim.SetBool("Aim", true);
                ray = camera.ScreenPointToRay(rotate.ScreenPoint);
                if (Physics.Raycast
                (
                    ray.origin,
                    ray.direction,
                    out raycastHit,
                    Mathf.Infinity,
                    LayerMask.GetMask("Default")
                ))
                {
                    mousePosition = raycastHit.point;
                    mousePosition.y = transform.position.y;

                    // Initialization
                    lookRotation = (mousePosition - transform.position).normalized;
                    Debug.DrawLine(transform.position, lookRotation, Color.red);
                    internalRigidbody.rotation = Quaternion.LookRotation(lookRotation);
                }

                yield return null;
            }
            anim.SetBool("Aim", false);
            rotate.ScreenPoint = Vector3.zero;
            coroutine = null;
        }

        protected float AngleBetweenPoints(Vector2 a, Vector2 b)
        {
            return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
        }

        #endregion
    }
}
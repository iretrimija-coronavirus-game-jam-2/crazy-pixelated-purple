﻿using UnityEngine;

namespace CPP.Character.Command
{
    public abstract class Command : MonoBehaviour, ICommand
    {
        public abstract void Execute();
    }
}

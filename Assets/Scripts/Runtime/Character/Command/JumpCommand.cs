﻿using System.Collections;
using CPP.Character.InputComponent;
using UnityEngine;

namespace CPP.Character.Command
{
    public class JumpCommand : Command
    {
        #region public variables

        public float jumpForce = 10f;
        public float fallMultiplier = 2.5f;
        public float jumpMultiplier = 2f;
        public bool legacyJump = false;

        #endregion

        #region protected variables

        protected IJumpInput jump;
        protected Rigidbody internalRigidbody;
        protected Coroutine jumpCoroutine;
        protected bool touchingGround = true;
        protected bool falling = false;
        protected Animator animator;

        #endregion

        #region public methods

        public override void Execute()
        {
            if (touchingGround && jumpCoroutine == null)
                jumpCoroutine = StartCoroutine(Jump());
        }

        #endregion

        #region protected methods

        protected void Awake()
        {
            jump = GetComponent<IJumpInput>();
            internalRigidbody = GetComponent<Rigidbody>();
            animator = GetComponent<Animator>();
        }

        protected void Update()
        {
            if (!legacyJump)
            {
                if (internalRigidbody.velocity.y < -0.1f) // Falling
                {
                    animator.SetBool("Falling", true);
                    animator.SetBool("Jumping", false);

                    falling = true;
                    touchingGround = false;
                    internalRigidbody.velocity +=
                        Vector3.up * (Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime);
                }
                else if (internalRigidbody.velocity.y > 0.1f) // Jumping
                {
                    animator.SetBool("Falling", false);
                    animator.SetBool("Jumping", true);

                    internalRigidbody.velocity +=
                        Vector3.up * (Physics.gravity.y * (jumpMultiplier - 1) * Time.deltaTime);
                }
                else if (falling) // Finish jump
                {
                    animator.SetBool("Falling", false);
                    animator.SetBool("Jumping", false);

                    touchingGround = true;
                    falling = false;
                }
            }
        }

        protected IEnumerator Jump()
        {
            internalRigidbody.velocity = Vector3.up * (jumpForce * jump.JumpButtonIncrement);
            if (!legacyJump)
                touchingGround = false;

            yield return null;
            jumpCoroutine = null;
        }

        #endregion
    }
}
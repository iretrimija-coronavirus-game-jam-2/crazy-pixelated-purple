﻿using System.Collections;
using UnityEngine;

namespace CPP.Character.Command
{
    public class LegacyJumpCommand : Command
    {
        #region public variables

        public float jumpForce = 10f;

        #endregion
        
        #region protected variables

        protected Rigidbody internalRigidbody;
        protected Coroutine jumpCoroutine;
        
        #endregion
        
        #region public methods

        public override void Execute()
        {
            if (jumpCoroutine == null)
                jumpCoroutine = StartCoroutine(Jump());
        }
        
        #endregion
        
        #region protected methods

        protected void Awake()
        {
            internalRigidbody = GetComponent<Rigidbody>();
        }

        protected IEnumerator Jump()
        {
            internalRigidbody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            
            yield return null;
            jumpCoroutine = null;
        }
        
        #endregion
    }
}
namespace CPP.Character.InputComponent
{
    public interface IInteractInput
    {
        bool IsPressingInteract { get; }
    }
}
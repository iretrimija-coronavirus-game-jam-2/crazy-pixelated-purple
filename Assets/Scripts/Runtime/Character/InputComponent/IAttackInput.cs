﻿using UnityEngine;

namespace CPP.Character.InputComponent
{
    public interface IAttackInput
    {
        Vector3 AttackDirection { get; }
        bool MayShoot { get;  }
    }
}
using UnityEngine;

namespace CPP.Character.InputComponent
{
    public interface IRotationInput
    {
        Vector2 ScreenPoint { get; set; }
        bool IsPressingRotation { get; }
    }
}
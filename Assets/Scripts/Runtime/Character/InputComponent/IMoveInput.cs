using UnityEngine;

namespace CPP.Character.InputComponent
{
    public interface IMoveInput
    {
        Vector3 MoveDirection { get; }
    }
}
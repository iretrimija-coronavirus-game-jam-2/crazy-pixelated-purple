﻿namespace CPP.Character.InputComponent
{
    public interface IJumpInput
    {
        float JumpButtonIncrement { get; }
    }
}
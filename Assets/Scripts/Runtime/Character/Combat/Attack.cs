﻿using CPP.Character.InputComponent;
using UnityEngine;

namespace CPP.Character.Combat
{
    public abstract class Attack : MonoBehaviour, IAttack
    {
        public abstract void ExecuteAttack(IAttackInput attackInput);
        public abstract void ExecuteAttack(Vector3 direction);
    }
}
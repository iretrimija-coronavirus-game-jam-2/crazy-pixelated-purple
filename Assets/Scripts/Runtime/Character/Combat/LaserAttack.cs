﻿using CPP.Character.InputComponent;
using UnityEngine;

namespace CPP.Character.Combat
{
    public class LaserAttack : Attack
    {
        #region public variables

        public GameObject LaserPrefab;
        public float force = 20f;

        #endregion

        #region public methods

        public override void ExecuteAttack(IAttackInput attackInput)
        {
            ExecuteAttack(transform.forward);
        }

        public override void ExecuteAttack(Vector3 direction)
        {
            if (LaserPrefab != null)
            {
                GameObject laserInstance = Instantiate(
                    LaserPrefab,
                    transform.position,
                    transform.rotation
                );

                Rigidbody rigidbody = laserInstance.GetComponentInChildren<Rigidbody>();
                rigidbody.AddForce(direction * force, ForceMode.Impulse);
            }
        }

        #endregion
    }
}
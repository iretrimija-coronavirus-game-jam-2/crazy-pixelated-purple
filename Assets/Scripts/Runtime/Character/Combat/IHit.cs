﻿namespace CPP.Character.Combat
{
    public interface IHit
    {
        int Damage { get; set; }
    }
}
﻿using CPP.Character.InputComponent;
using UnityEngine;

namespace CPP.Character.Combat
{
    public interface IAttack
    {
        void ExecuteAttack(IAttackInput attackInput);
        void ExecuteAttack(Vector3 direction);
    }
}
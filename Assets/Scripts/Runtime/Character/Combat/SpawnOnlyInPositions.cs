﻿using UnityEngine;

namespace CPP.Character.Combat
{
    public class SpawnOnlyInPositions : MonoBehaviour
    {
        #region public variables

        public GameObject enemyToInstantiate;

        public Transform[] positions = new Transform[0];

        #endregion

        #region protected variables

        protected GameObject enemy;

        #endregion

        #region public methods

        [ContextMenu("InstantiateEnemies")]
        public void InstantiateEnemies()
        {
            foreach (Transform position in positions)
            {
                enemy = Instantiate(enemyToInstantiate, position.position, Quaternion.identity);
            }
        }

        #endregion

        #region protected methods

        protected void Start()
        {
            if (positions.Length == 0)
            {
                Transform[] transforms = GetComponentsInChildren<Transform>();
                positions = new Transform[transforms.Length - 1];
                for (byte index = 0; index < positions.Length; ++index)
                {
                    positions[index] = transforms[index + 1];
                }
            }
        }

        #endregion
    }
}
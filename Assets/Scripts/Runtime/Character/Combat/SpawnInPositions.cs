﻿using UnityEngine;

namespace CPP.Character.Combat
{
    public class SpawnInPositions : MonoBehaviour
    {
        #region public variables

        public GameObject enemyToInstantiate;

        public GameObject[] enemiesToDestroy = new GameObject[0];

        public Transform[] positions = new Transform[0];

        #endregion

        #region protected variables

        protected GameObject enemy;
        protected bool spawn = true;
        protected int counter = 0;
        protected int currentCounter = 0;

        #endregion

        #region public methods

        [ContextMenu("InstantiateEnemies")]
        public void InstantiateEnemies()
        {
            if (spawn)
            {
                enemiesToDestroy = new GameObject[positions.Length];
                byte index = 0;
                foreach (Transform position in positions)
                {
                    enemy = Instantiate(enemyToInstantiate, position.position, Quaternion.identity);
                    enemiesToDestroy[index] = enemy;
                    ++index;
                }

                AddEnemiesAndConfigure();
            }
        }

        public void ToogleSpawn()
        {
            spawn = !spawn;
        }

        public void AddCounter()
        {
            ++currentCounter;
            if (currentCounter >= counter)
            {
                InstantiateEnemies();
            }
        }

        #endregion

        #region protected methods

        protected void Start()
        {
            if (positions.Length == 0)
            {
                Transform[] transforms = GetComponentsInChildren<Transform>();
                positions = new Transform[transforms.Length - 1];
                for (byte index = 0; index < positions.Length; ++index)
                {
                    positions[index] = transforms[index + 1];
                }
            }

            AddEnemiesAndConfigure();
        }

        protected void AddEnemiesAndConfigure()
        {
            counter = enemiesToDestroy.Length;
            currentCounter = 0;
            foreach (GameObject enemy in enemiesToDestroy)
            {
                CharacterHealth health = enemy.GetComponent<CharacterHealth>();
                health.OnDeath.AddListener(() => AddCounter());
            }
        }

        protected void OnDisable()
        {
            spawn = false;
        }

        #endregion
    }
}
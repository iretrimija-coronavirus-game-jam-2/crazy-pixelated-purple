﻿using CPP.Character.Command;
using UnityEngine;
using UnityEngine.InputSystem;

namespace CPP.Camera
{
    public class CharacterIntroInput : MonoBehaviour
    {
        #region public variables

        public Command inputFreeCommand;

        #endregion

        #region protected variables

        protected InputMaster input;

        #endregion

        #region public methods

        public void FreeInput(InputAction.CallbackContext context)
        {
            Vector2 value = context.ReadValue<Vector2>();
            if (inputFreeCommand != null)
                inputFreeCommand.Execute();
        }

        #endregion

        #region protected methods

        protected void Awake()
        {
            // Initialize
            input = new InputMaster();

            // Subscribe to events
            input.Player.Movement.performed += FreeInput;
        }

        protected void OnEnable()
        {
            input.Enable();
        }

        protected void OnDisable()
        {
            input.Disable();
        }

        protected void Reset()
        {
            inputFreeCommand = GetComponent<InputFreeCommand>();
        }

        protected void Start()
        {
            Reset();
        }

        #endregion
    }
}
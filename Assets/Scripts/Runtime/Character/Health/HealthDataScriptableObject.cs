﻿using UnityEngine;

namespace CPP.Character.Health
{
    [CreateAssetMenu(
        fileName = "HealthData",
        menuName = "ScriptableObjects/Health/HealthData",
        order = 1)
    ]
    public class HealthDataScriptableObject : ScriptableObject
    {
        public int maxHealth = 3;
    }
}
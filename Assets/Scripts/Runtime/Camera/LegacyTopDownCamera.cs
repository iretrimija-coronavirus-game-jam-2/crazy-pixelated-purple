﻿using UnityEngine;

namespace CPP.Camera
{
    public class LegacyTopDownCamera : MonoBehaviour
    {
        #region public variables

        public Transform target;

        public float height = 10f;
        public float distance = 20f;
        public float angle = 45f;
        public float movementSpeed = 1f;

        #endregion

        #region protected variables
        
        // Auxiliary variables
        protected Vector3 velocity;
        protected Color gizmosColor = new Color(1f, 1f, 0f, 0.5f);
        protected Vector3 worldPosition;
        protected Vector3 rotatedVector;
        protected Vector3 flatTargetPosition;
        protected Vector3 finalPosition;

        #endregion

        #region protected methods

        /// <summary>
        /// Start is called before the first frame update
        /// </summary>
        protected void Start()
        {
            HandleCamera();
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        protected void Update()
        {
            HandleCamera();
        }

        protected virtual void HandleCamera()
        {
            if (target == null)
                Debug.LogError("You have to attach a target to follow!");
            else
            {
                // Vector world position
                worldPosition = Vector3.forward * -distance + Vector3.up * height;

                // Rotated vector
                rotatedVector = Quaternion.AngleAxis(angle, Vector3.up) * worldPosition;

                // Move position
                flatTargetPosition = target.position;
                flatTargetPosition.y = 0f;
                finalPosition = flatTargetPosition + rotatedVector;

                // Set the new position to the camera
                /*transform.position = Vector3.SmoothDamp(
                    transform.position,
                    finalPosition,
                    ref velocity,
                    movementSpeed
                );*/
                transform.position = finalPosition;
                transform.LookAt(flatTargetPosition);
            }
        }

        protected void OnDrawGizmos()
        {
            Gizmos.color = gizmosColor;
            if (target != null)
            {
                Gizmos.DrawLine(transform.position, target.position);
                Gizmos.DrawSphere(target.position, 0.5f);
            }

            Gizmos.DrawSphere(transform.position, 0.5f);
        }

        #endregion
    }
}
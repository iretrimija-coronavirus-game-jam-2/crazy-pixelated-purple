﻿using UnityEngine;

namespace CPP.Camera
{
    public class TopDownCamera : MonoBehaviour
    {
        #region public variables

        public Transform target;
        public TopDownCameraDataScriptableObject data;

        #endregion

        #region protected variables

        // Auxiliary variables
        protected Color gizmosColor = new Color(1f, 1f, 0f, 0.5f);
        protected Vector3 lastPlayerPos;
        protected Vector3 moveCurrentVelocity;
        protected float heightCurrentVelocity;

        #endregion

        #region public methods

        public void SetCameraData(TopDownCameraDataScriptableObject newData)
        {
            data = newData;
        }

        public void SetNewTarget(Transform newTarget)
        {
            target = newTarget;
        }

        #endregion

        #region protected methods

        /// <summary>
        /// Start is called before the first frame update
        /// </summary>
        protected void Start()
        {
            if (data == null)
                data = new TopDownCameraDataScriptableObject();

            lastPlayerPos = target.transform.position;
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        protected void FixedUpdate()
        {
            HandleCamera();
        }

        protected virtual void HandleCamera()
        {
            if (target == null)
                Debug.LogError("You have to attach a target to follow!");
            else
            {
                lastPlayerPos = target.transform.position + data.offset;

                /* ### Movement ### */

                // X- and Z-axis smooth follow
                if (data.applySmooth)
                {
                    Vector3 moveSmoothed = Vector3.SmoothDamp(
                        transform.position,
                        lastPlayerPos -
                        Vector3.back * -data.followBehindDistance -
                        Vector3.right * -data.followRightDistance,
                        ref moveCurrentVelocity,
                        data.moveSmoothTime
                    );

                    // Y-axis smooth follow
                    float heightSmoothed = Mathf.SmoothDamp(
                        transform.position.y,
                        data.height,
                        ref heightCurrentVelocity,
                        data.heightSmoothTime
                    );

                    // Apply X-,Y-,Z-axis smoothing
                    transform.position = new Vector3(moveSmoothed.x, heightSmoothed, moveSmoothed.z);
                }
                else
                {
                    // Apply X-,Y-,Z-axis smoothing
                    transform.position = new Vector3(lastPlayerPos.x, data.height, lastPlayerPos.z);
                }


                /* ### Rotation ### */

                // Find the point we are rotating around
                Vector3 toTarget =
                (
                    lastPlayerPos + Vector3.back * data.lookThisFarInFront +
                    Vector3.right * data.lookThisFarInRight
                ) - transform.position;
                Quaternion targetRotation = Quaternion.LookRotation(toTarget);

                // Find and apply a lerped rotation
                transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, data.rotationSmoothTime);
            }
        }

        protected void OnDrawGizmos()
        {
            Gizmos.color = gizmosColor;
            if (target != null)
            {
                Gizmos.DrawLine(transform.position, target.position);
                Gizmos.DrawSphere(target.position, 0.5f);
            }

            Gizmos.DrawSphere(transform.position, 0.5f);
        }

        #endregion
    }
}
﻿using UnityEngine;

namespace CPP.Camera
{
    [RequireComponent(typeof(BoxCollider))]
    public class TopDownCameraDataChanger : MonoBehaviour
    {
        #region public variables

        public const string PLAYER_TAG = "Player";
        public const string PLAYER_INTRO_TAG = "PlayerStart";

        #endregion

        #region public variables

        [Header("Parameters")] public TopDownCamera topDownCamera;

        public TopDownCameraDataScriptableObject topDownCameraData;

        [Header("Visual editor")] public Color gizmosColor = new Color(0f, 1f, 1f, 0.5f);

        #endregion

        #region protected variables

        protected static TopDownCameraDataScriptableObject mainTopDownCameraData;

        protected Collider cameraCollider;
        protected BoxCollider boxCollider;
        protected static uint insideCollider = 0;
        protected bool playerInside = false;

        #endregion

        #region public methods

        public void ChangeData()
        {
            if (topDownCamera != null && topDownCamera.data != topDownCameraData)
            {
                topDownCamera.SetCameraData(topDownCameraData);
                cameraCollider.isTrigger = topDownCamera.data.colliderTrigger;
            }
        }

        public void ChangeOriginalData()
        {
            if (topDownCamera != null &&
                mainTopDownCameraData != null
            )
            {
                topDownCamera.SetCameraData(mainTopDownCameraData);
                cameraCollider.isTrigger = topDownCamera.data.colliderTrigger;
            }
        }

        #endregion

        #region protected methods

        // Start is called before the first frame update
        protected void Start()
        {
            if (topDownCamera == null)
                topDownCamera = UnityEngine.Camera.main.GetComponent<TopDownCamera>();

            mainTopDownCameraData = topDownCamera.data;
            cameraCollider = topDownCamera.GetComponent<Collider>();
        }

        protected void Reset()
        {
            topDownCamera = FindObjectOfType<TopDownCamera>();
        }

        protected void OnTriggerEnter(Collider other)
        {
            if (other.tag.Equals(PLAYER_TAG) || other.tag.Equals(PLAYER_INTRO_TAG))
            {
                playerInside = true;
                ++insideCollider;
                ChangeData();
            }
        }

        protected void OnTriggerExit(Collider other)
        {
            if (other.tag.Equals(PLAYER_TAG) || other.tag.Equals(PLAYER_INTRO_TAG))
            {
                playerInside = false;
                --insideCollider;
                if (insideCollider == 0)
                    ChangeOriginalData();
            }
        }

        protected void OnDisable()
        {
            if (insideCollider > 0 && playerInside)
            {
                playerInside = false;
                --insideCollider;
                if (insideCollider == 0)
                    ChangeOriginalData();
            }
        }

        protected void OnDrawGizmos()
        {
            if (boxCollider == null)
                boxCollider = GetComponent<BoxCollider>();

            Gizmos.color = gizmosColor;
            Gizmos.DrawCube(transform.position + boxCollider.center, boxCollider.size);
        }

        #endregion
    }
}
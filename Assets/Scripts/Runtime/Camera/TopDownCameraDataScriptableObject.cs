﻿using UnityEngine;

namespace CPP.Camera
{

    [CreateAssetMenu(
        fileName = "TopDownCameraData",
        menuName = "ScriptableObjects/Camera/TopDownCameraData",
        order = 1)
    ]
    public class TopDownCameraDataScriptableObject : ScriptableObject
    {
        [Header("Smooth Parameters")]
        public float moveSmoothTime = 1f; // Lower is slower
        public float heightSmoothTime = 1f; // Lower is slower
        public float rotationSmoothTime = 0.1f; // Lower is slower
        public bool applySmooth = true;
        
        [Header("Camera Parameters")]
        public float followBehindDistance = -8f;
        public float followRightDistance = 0f;
        public float lookThisFarInFront = 9f;
        public float lookThisFarInRight = 0f;
        public float height = 10f;
        public Vector3 offset = new Vector3(0, 0, 5);
        public bool colliderTrigger = false;
    }
}
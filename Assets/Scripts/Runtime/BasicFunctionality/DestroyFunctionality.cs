﻿using UnityEngine;

namespace CPP.BasicFunctionality
{
    public class DestroyFunctionality : MonoBehaviour, IFunctionality
    {
        #region public methods
        
        public void Execute()
        {
            Destroy(gameObject);
        }
        
        #endregion
    }
}
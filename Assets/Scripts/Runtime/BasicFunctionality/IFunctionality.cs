﻿namespace CPP.BasicFunctionality
{
    public interface IFunctionality
    {
        void Execute();
    }
}
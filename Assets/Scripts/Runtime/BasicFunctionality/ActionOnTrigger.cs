﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionOnTrigger : MonoBehaviour
{
    #region public variables

    public GameObject[] ActivateObjects; //Activate Objects
    public GameObject[] DeactivateObjects; //Delete Objects
    public GameObject[] ActivateColliderObjects; //Activate Colliders
    public GameObject[] ActivateOpenFunction;
    public GameObject[] ActivateCloseFunction;
    public bool deletable = true;

    #endregion

    #region protected variables

    #endregion

    // Start is called before the first frame update
    void Start()
    {
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            for (int i = 0; i < DeactivateObjects.Length; i++)
            {
                DeactivateObjects[i].SetActive(false);
            }

            for (int i = 0; i < ActivateObjects.Length; i++)
            {
                ActivateObjects[i].SetActive(true);
            }

            for (int i = 0; i < ActivateColliderObjects.Length; i++)
            {
                ActivateColliderObjects[i].GetComponent<Collider>().enabled = true;
            }

            for (int i = 0; i < ActivateOpenFunction.Length; i++)
            {
                ActivateOpenFunction[i].SendMessage("Open");
            }

            for (int i = 0; i < ActivateCloseFunction.Length; i++)
            {
                ActivateCloseFunction[i].SendMessage("Close");
            }

            if (deletable)
            {
                Destroy(this.gameObject);
            }
            else
            {
                //If cant be destroyed will be deactivated to avoid showing messages on the ui
                GetComponent<Collider>().enabled = false;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}
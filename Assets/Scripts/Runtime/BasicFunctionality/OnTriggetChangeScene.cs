﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class OnTriggetChangeScene : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            SceneManager.LoadScene("OUTRO");
        }
    }
}
﻿using Bonsai;
using Bonsai.Core;
using UnityEngine;
using UnityEngine.AI;

namespace CPP.BehaviourTree
{
    [BonsaiNode("Tasks/", "Arrow")]
    public class ExistMinimumDistanceToShoot : Task
    {
        #region public variables

        public string tagTarget = "Player";
        public float targetDistance = 8f;
        public float threshold = 1f;
        public bool debug = false;

        #endregion

        #region protected variables

        protected GameObject objectToShoot;
        protected Vector3 targetPosition;
        protected Vector3 position;
        protected Vector3 destinationPoint;
        protected float distanceFromDestination;

        #endregion

        #region public methods

        public override Status Run()
        {
            targetPosition = objectToShoot.transform.position;
            position = Actor.transform.position;
            destinationPoint = targetPosition + (position - targetPosition).normalized * targetDistance;

            distanceFromDestination = (destinationPoint - position).z;

            if (debug)
            {
                Debug.Log("Distance: " + distanceFromDestination);
                Debug.DrawLine(targetPosition, destinationPoint, Color.red);
            }

            if (distanceFromDestination < threshold)
                return Status.Success;
            else
                return Status.Failure;
        }

        public override void OnStart()
        {
            // Initialization
            objectToShoot = GameObject.FindGameObjectWithTag(tagTarget);
        }

        #endregion
    }
}
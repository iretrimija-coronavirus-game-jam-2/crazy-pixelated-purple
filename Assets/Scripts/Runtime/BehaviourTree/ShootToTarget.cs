﻿using Bonsai;
using Bonsai.Core;
using CPP.Character.Combat;
using UnityEngine;
using UnityEngine.AI;

namespace CPP.BehaviourTree
{
    [BonsaiNode("Tasks/", "Arrow")]
    public class ShootToTarget : Task
    {
        #region public variables

        public string tagTarget = "Player";

        #endregion

        #region protected variables

        protected GameObject objectToShoot;
        protected NavMeshAgent agent;
        protected Vector3 targetPosition;
        protected Vector3 position;
        protected Vector3 destinationVector;
        
        protected IAttack attack;

        #endregion

        #region public methods

        public override Status Run()
        {
            destinationVector = (objectToShoot.transform.position - Actor.transform.position).normalized;
            attack.ExecuteAttack(destinationVector);
            SoundManager.PlaySound("enemyShoot");

            return Status.Success;
        }

        public override void OnStart()
        {
            // Initialization
            objectToShoot = GameObject.FindGameObjectWithTag(tagTarget);
            attack = Actor.GetComponentInChildren<IAttack>();
        }

        #endregion
    }
}
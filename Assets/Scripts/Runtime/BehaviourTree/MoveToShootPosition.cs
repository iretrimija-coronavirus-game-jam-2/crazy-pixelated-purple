﻿using Bonsai;
using Bonsai.Core;
using UnityEngine;
using UnityEngine.AI;

namespace CPP.BehaviourTree
{
    [BonsaiNode("Tasks/", "Arrow")]
    public class MoveToShootPosition : Task
    {
        #region public variables

        public string tagTarget = "Player";
        public float targetDistance = 3f;

        #endregion

        #region protected variables

        protected GameObject objectToShoot;
        protected NavMeshAgent agent;
        protected Vector3 targetPosition;
        protected Vector3 position;
        protected Vector3 destinationPoint;

        #endregion

        #region public methods

        public override Status Run()
        {
            targetPosition = objectToShoot.transform.position;
            position = Actor.transform.position;
            destinationPoint = targetPosition + (position - targetPosition).normalized * targetDistance;

            // Move
            agent.destination = destinationPoint;

            return Status.Success;
        }

        public override void OnStart()
        {
            // Initialization
            objectToShoot = GameObject.FindGameObjectWithTag(tagTarget);
            agent = Actor.GetComponentInChildren<NavMeshAgent>();
        }

        #endregion
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntroController : MonoBehaviour
{
    public Text text;
    private string go = "Press [E] to start";
    string[] s = {"Level designer\n[Irene Marina Barbé]","Code maker\n[Javier Osuna Herrera]","3D Models (except main robot)\n[Miguel Angel García]","Lighting Artist Support\n[Guadalupe Hermoso]", "Music Composer\n[Daniel Barbé Crespo]"};
    int index = 0;
    public GameObject black;
    float temp = 1f;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("DisableText",1f);
        Invoke("Fadeout",2f);
        //Color color = black.GetComponent<MeshRenderer>().material.color ;
        //color.a -= 1f;
        //black.GetComponent<MeshRenderer>().material.color = color;
        Invoke("ShowText",8);
        Invoke("ShowText",12);
        Invoke("ShowText",16);
        Invoke("ShowText",20);
        Invoke("ShowText",24);
        Invoke("Menu",32);
    }

    void Fadeout(){
        StartCoroutine(blacking());
    }

    void Menu(){
        text.text = go;
    }

    void ShowText(){
        StartCoroutine(TypeText(s[index]));
    }

    void DisableText(){
        text.text = "";
    }

    IEnumerator blacking () {
         for(int i = 0;i<100;i++){
            //temp = temp - 0.01f;
            Color color = black.GetComponent<MeshRenderer>().material.color ;
            color.a -= 0.01f;
            black.GetComponent<MeshRenderer>().material.color = color;
            yield return new WaitForSeconds (0.15f);
        }
     }

    IEnumerator TypeText (string message) {
         foreach (char letter in message.ToCharArray()) {
             text.text += letter;
             yield return new WaitForSeconds (0.02f);
         }
         index++;
         Invoke("DisableText",2.5f);
     }

    // Update is called once per frame
    void Update()
    {
        
    }
}

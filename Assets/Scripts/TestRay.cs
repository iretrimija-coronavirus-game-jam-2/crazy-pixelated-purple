﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TestRay : MonoBehaviour
{
    public GameObject cube;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Ray ray = GetComponent<Camera>().ScreenPointToRay(Mouse.current.position.ReadValue());
        Debug.Log("-- " + Mouse.current.position.ReadValue());
        if (Physics.Raycast(ray, out hit)) {
            Transform objectHit = hit.transform;
            cube.transform.position = hit.point;

        }
    }
}

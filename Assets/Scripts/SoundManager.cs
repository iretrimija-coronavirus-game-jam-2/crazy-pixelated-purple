﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static AudioClip playerShoot;
    public static AudioClip enemyShoot;
    public static AudioClip gunLoaded;
    public static AudioClip keyboard;
    public static AudioClip switchActive;

    static AudioSource audioSrc;

    // Start is called before the first frame update
    void Start()
    {
        playerShoot = Resources.Load<AudioClip>("Shoot_2");
        enemyShoot = Resources.Load<AudioClip>("Shoot_1");
        gunLoaded = Resources.Load<AudioClip>("GunLoaded");
        keyboard = Resources.Load<AudioClip>("Keyboard");
        switchActive = Resources.Load<AudioClip>("SwitchActive");
        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void PlaySound (string clip)
    {
        switch (clip)
        {
            case "playerShoot":
                audioSrc.PlayOneShot(playerShoot);
                break;
            case "enemyShoot":
                audioSrc.PlayOneShot(enemyShoot);
                break;
            case "gunLoaded":
                audioSrc.PlayOneShot(gunLoaded);
                break;
            case "keyboard":
                audioSrc.PlayOneShot(keyboard);
                break;
            case "switchActive":
                audioSrc.PlayOneShot(switchActive);
                break;
        }
    }
}

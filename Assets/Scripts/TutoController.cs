﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutoController : MonoBehaviour
{
    GameObject master;
    // Start is called before the first frame update
    void Start()
    {
        master = GameObject.Find("TutoMaster");
    }

    void OnDestroy()
    {
        master.SendMessage("Show", "");
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            master.SendMessage("InteractTuto", this.gameObject.name);
            //Invoke("DisableText", 5f);
        }
    }

    void OnTriggerExit(Collider other)
    {
        master.SendMessage("Show", "");
        //Close();
    }

    public void Close()
    {
        Destroy(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
